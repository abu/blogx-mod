<?php if ($WHERE_AM_I !== 'category' && !$page->isStatic() && !$url->notFound()): ?>
	<p>Category: <a href="<?php echo DOMAIN_CATEGORIES . $page->categoryKey(); ?>"><?php echo $page->category(); ?></a></p>
<?php endif ?>
