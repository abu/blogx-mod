# blogx-mod

Modified version of the original [Bludit](https://www.bludit.com/) theme.

## Modifications

- Added Codeberg fork-ribbon.css
- Added Syntax highlighting, from https://highlightjs.org/
- Don't transform site title to uppercase
- Omit static child pages in navbar
- Reduce width of article separator
- Display article-category below article, unless in category listing, static- and error-pages.
- Added Icons for Codeberg and Hubzilla.
